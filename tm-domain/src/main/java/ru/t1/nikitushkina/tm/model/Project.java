package ru.t1.nikitushkina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.model.IWBS;
import ru.t1.nikitushkina.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUId = 1;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date created = new Date();

    public Project(@NotNull final String name) {
        this.name = name;
    }

    public Project(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public Project(
            @NotNull final String name,
            @NotNull final Status status
    ) {
        this.name = name;
        this.status = status;
    }

    public Project(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status
    ) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
