package ru.t1.nikitushkina.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.Task;

@NoArgsConstructor
public class TaskStartByIdResponse extends AbstractResponse {
}
