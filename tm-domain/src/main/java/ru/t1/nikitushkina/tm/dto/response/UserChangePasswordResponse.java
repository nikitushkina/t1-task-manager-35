package ru.t1.nikitushkina.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.User;

@NoArgsConstructor
public class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@Nullable User user) {
        super(user);
    }

}
