package ru.t1.nikitushkina.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectStartByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public ProjectStartByIdRequest(@Nullable String token) {
        super(token);
    }

}
