package ru.t1.nikitushkina.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class UserChangePasswordRequest extends AbstractUserRequest {

    @Nullable
    private String password;

    public UserChangePasswordRequest(@Nullable String token) {
        super(token);
    }

}
