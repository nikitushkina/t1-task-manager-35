package ru.t1.nikitushkina.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static String USER_TASK1_NAME = "Task 01";

    @NotNull
    public final static String USER_TASK1_DESCRIPTION = "Description 01";

    @NotNull
    public final static String USER_TASK2_NAME = "Task 02";

    @NotNull
    public final static String USER_TASK2_DESCRIPTION = "Description 02";

    @NotNull
    public final static String USER_TASK3_NAME = "Task 03";

    @NotNull
    public final static String USER_TASK3_DESCRIPTION = "Description 03";


}
