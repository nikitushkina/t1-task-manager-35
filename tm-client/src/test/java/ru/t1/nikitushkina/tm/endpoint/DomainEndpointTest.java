package ru.t1.nikitushkina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nikitushkina.tm.api.endpoint.IAuthEndpoint;
import ru.t1.nikitushkina.tm.api.endpoint.IDomainEndpoint;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.dto.request.*;
import ru.t1.nikitushkina.tm.marker.IntegrationCategory;
import ru.t1.nikitushkina.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class DomainEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IDomainEndpoint domainEndpointClient = IDomainEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.login(loginRequest).getToken();
    }

    @Test
    public void loadDataBackup() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataBackup(request));
    }

    @Test
    public void saveDataBackup() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataBackup(request));
    }

    @Test
    public void loadDataBase64() {
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataBase64(request));
    }

    @Test
    public void saveDataBase64() {
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataBase64(request));
    }

    @Test
    public void loadDataBinary() {
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataBinary(request));
    }

    @Test
    public void saveDataBinary() {
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataBinary(request));
    }

    @Test
    public void loadDataJsonFasterXml() {
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataJsonFasterXml(request));
    }

    @Test
    public void saveDataJsonFasterXml() {
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataJsonFasterXml(request));
    }

    @Test
    public void loadDataJsonJaxB() {
        @NotNull final DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataJsonJaxB(request));
    }

    @Test
    public void saveDataJsonJaxB() {
        @NotNull final DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataJsonJaxB(request));
    }

    @Test
    public void loadDataXmlFasterXml() {
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataXmlFasterXml(request));
    }

    @Test
    public void saveDataXmlFasterXml() {
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataXmlFasterXml(request));
    }

    @Test
    public void loadDataXmlJaxB() {
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataXmlJaxB(request));
    }

    @Test
    public void saveDataXmlJaxB() {
        @NotNull final DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataXmlJaxB(request));
    }

    @Test
    public void loadDataYamlFasterXml() {
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.loadDataYamlFasterXml(request));
    }

    @Test
    public void saveDataYamlFasterXml() {
        @NotNull final DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.saveDataYamlFasterXml(request));
    }

}
