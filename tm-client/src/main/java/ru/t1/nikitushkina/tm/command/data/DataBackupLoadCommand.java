package ru.t1.nikitushkina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.DataBackupLoadRequest;
import ru.t1.nikitushkina.tm.enumerated.Role;

public class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-backup-load";

    @NotNull
    public static final String DESCRIPTION = "Load backup data from xml.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataBackupLoadRequest request = new DataBackupLoadRequest(getToken());
        getDomainEndpointClient().loadDataBackup(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
