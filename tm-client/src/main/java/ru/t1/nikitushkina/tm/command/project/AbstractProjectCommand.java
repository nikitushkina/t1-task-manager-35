package ru.t1.nikitushkina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.endpoint.IProjectEndpoint;
import ru.t1.nikitushkina.tm.command.AbstractCommand;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.model.Project;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    protected IProjectEndpoint getProjectEndpoint() {
        return serviceLocator.getProjectEndpointClient();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderProjects(@NotNull final List<Project> projects) {
        int index = 1;
        for (@NotNull final Project project : projects) {
            showProject(project);
            index++;
        }
    }

    protected void showProject(@NotNull final Project project) {
        if (project == null) return;
        System.out.println("USER ID: " + project.getUserId());
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS:" + Status.toName(project.getStatus()));
    }

}
