package ru.t1.nikitushkina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.DataYamlSaveFasterXmlRequest;
import ru.t1.nikitushkina.tm.enumerated.Role;

public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-yaml";

    @NotNull
    public static final String DESCRIPTION = "Save data in yaml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        @NotNull DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(getToken());
        getDomainEndpointClient().saveDataYamlFasterXml(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
