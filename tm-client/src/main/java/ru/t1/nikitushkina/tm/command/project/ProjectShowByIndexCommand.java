package ru.t1.nikitushkina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.ProjectGetByIndexRequest;
import ru.t1.nikitushkina.tm.dto.response.ProjectGetByIndexResponse;
import ru.t1.nikitushkina.tm.model.Project;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Show project by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final ProjectGetByIndexResponse response = getProjectEndpoint().getProjectByIndex(request);
        @Nullable final Project project = response.getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
