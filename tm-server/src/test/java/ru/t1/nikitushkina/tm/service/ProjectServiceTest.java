package ru.t1.nikitushkina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nikitushkina.tm.api.repository.IProjectRepository;
import ru.t1.nikitushkina.tm.api.service.IProjectService;
import ru.t1.nikitushkina.tm.comparator.NameComparator;
import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nikitushkina.tm.exception.field.DescriptionEmptyException;
import ru.t1.nikitushkina.tm.exception.field.IdEmptyException;
import ru.t1.nikitushkina.tm.exception.field.IndexIncorrectException;
import ru.t1.nikitushkina.tm.exception.field.NameEmptyException;
import ru.t1.nikitushkina.tm.exception.user.UserIdEmptyException;
import ru.t1.nikitushkina.tm.marker.UnitCategory;
import ru.t1.nikitushkina.tm.model.Project;
import ru.t1.nikitushkina.tm.repository.ProjectRepository;

import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.t1.nikitushkina.tm.constant.ProjectTestData.*;
import static ru.t1.nikitushkina.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final IProjectService service = new ProjectService(repository);

    @Test
    public void add() {
        Assert.assertNotNull(service.add(ADMIN_PROJECT1));
        @Nullable final Project project = service.findOneById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, ADMIN_PROJECT1);
        });
        Assert.assertNotNull(service.add(ADMIN_TEST.getId(), ADMIN_PROJECT1));
        @Nullable final Project project = service.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(service.add(ADMIN_PROJECT_LIST));
        for (final Project project : ADMIN_PROJECT_LIST)
            Assert.assertEquals(project, service.findOneById(project.getId()));
    }

    @After
    public void after() {
        repository.removeAll(PROJECT_LIST);
    }

    @Before
    public void before() {
        repository.add(USER_PROJECT1);
        repository.add(USER_PROJECT2);
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById(null, USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById("", USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(USER_TEST.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(USER_TEST.getId(), "", status);
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.changeProjectStatusById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, status);
        });
        service.changeProjectStatusById(USER_TEST.getId(), USER_PROJECT1.getId(), status);
        Assert.assertNotNull(USER_PROJECT1);
        Assert.assertEquals(status, USER_PROJECT1.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final Status status = Status.COMPLETED;
        @Nullable final Project project = service.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        final int index = service.findAll().indexOf(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusByIndex(null, index, status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusByIndex("", index, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeProjectStatusByIndex(USER_TEST.getId(), null, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeProjectStatusByIndex(USER_TEST.getId(), -1, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeProjectStatusByIndex(USER_TEST.getId(), service.getSize(), status);
        });
        service.changeProjectStatusByIndex(USER_TEST.getId(), index, status);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void clear() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT_LIST);
        emptyService.clear();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void clearByUserId() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.clear(null);
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT1);
        emptyService.add(USER_PROJECT2);
        emptyService.clear(USER_TEST.getId());
        Assert.assertEquals(0, emptyService.getSize(USER_TEST.getId()));
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, ADMIN_PROJECT1.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", ADMIN_PROJECT1.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), "");
        });
        @NotNull final Project project = service.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName());
        Assert.assertEquals(project, service.findOneById(ADMIN_TEST.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), null, ADMIN_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), "", ADMIN_PROJECT1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName(), "");
        });
        @NotNull final Project project = service.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertEquals(project, service.findOneById(ADMIN_TEST.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_PROJECT_ID);
        });
        Assert.assertFalse(service.existsById(USER_TEST.getId(), null));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), ""));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(USER_TEST.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT_LIST);
        Assert.assertEquals(USER_PROJECT_LIST, emptyService.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(USER_PROJECT_LIST, service.findAll(USER_TEST.getId()));
    }

    @Test
    public void findAllComparator() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository);
        emptyService.add(USER_PROJECT_LIST);
        emptyService.add(ADMIN_PROJECT_LIST);
        @Nullable Comparator comparator = null;
        Assert.assertEquals(PROJECT_LIST, emptyService.findAll(comparator));
        comparator = NameComparator.INSTANCE;
        Assert.assertEquals(SORTED_PROJECT_LIST, emptyService.findAll(comparator));
    }

    @Test
    public void findAllComparatorByUserId() {
        @Nullable Comparator comparator = null;
        Assert.assertEquals(USER_PROJECT_LIST, service.findAll(USER_TEST.getId(), comparator));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            service.findAll("", comparatorInner);
        });
        comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USER_PROJECT_LIST.stream().sorted(comparator).collect(Collectors.toList()), service.findAll(USER_TEST.getId(), comparator));
    }

    @Test
    public void findAllSortByUserId() {
        @Nullable Sort sort = null;
        Assert.assertEquals(USER_PROJECT_LIST, service.findAll(USER_TEST.getId(), sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            service.findAll("", sortInner);
        });
        sort = Sort.BY_NAME;
        Assert.assertEquals(USER_PROJECT_LIST.stream().sorted(sort.getComparator()).collect(Collectors.toList()), service.findAll(USER_TEST.getId(), sort.getComparator()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_TEST.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USER_PROJECT1.getId());
        });
        Assert.assertNull(service.findOneById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.findOneById(USER_TEST.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(null);
        });
        final int index = service.findAll().indexOf(USER_PROJECT1);
        @Nullable final Project project = service.findOneByIndex(index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByIndexByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USER_TEST.getId(), null);
        });
        final int index = service.findAll().indexOf(USER_PROJECT1);
        @Nullable final Project project = service.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void getSize() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize());
        emptyService.add(ADMIN_PROJECT1);
        Assert.assertEquals(1, emptyService.getSize());
    }

    @Test
    public void getSizeByUserId() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize("");
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize(ADMIN_TEST.getId()));
        emptyService.add(ADMIN_PROJECT1);
        emptyService.add(USER_PROJECT1);
        Assert.assertEquals(1, emptyService.getSize(ADMIN_TEST.getId()));
    }

    @Test
    public void remove() {
        Assert.assertNull(service.remove(null));
        @Nullable final Project createdProject = service.add(ADMIN_PROJECT1);
        @Nullable final Project removedProject = service.remove(createdProject);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeAll() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(PROJECT_LIST);
        emptyService.removeAll(PROJECT_LIST);
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertNull(service.removeById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project createdProject = service.add(ADMIN_PROJECT1);
        @Nullable final Project removedProject = service.removeById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_TEST.getId(), "");
        });
        Assert.assertNull(service.removeById(ADMIN_TEST.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertNull(service.removeById(ADMIN_TEST.getId(), USER_PROJECT1.getId()));
        @Nullable final Project createdProject = service.add(ADMIN_PROJECT1);
        @Nullable final Project removedProject = service.removeById(ADMIN_TEST.getId(), createdProject.getId());
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(null);
        });
        @Nullable final Project createdProject = service.add(ADMIN_PROJECT1);
        final int index = service.findAll().indexOf(createdProject);
        @Nullable final Project removedProject = service.removeByIndex(index);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIndexByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(USER_TEST.getId(), null);
        });
        @Nullable final Project createdProject = service.add(ADMIN_PROJECT1);
        final int index = service.findAll(ADMIN_TEST.getId()).indexOf(createdProject);
        @Nullable final Project removedProject = service.removeByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", null);
        });
        Assert.assertNull(service.remove(ADMIN_TEST.getId(), null));
        @Nullable final Project createdProject = service.add(ADMIN_PROJECT1);
        @Nullable final Project removedProject = service.remove(ADMIN_TEST.getId(), createdProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void set() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT_LIST);
        emptyService.set(ADMIN_PROJECT_LIST);
        Assert.assertEquals(ADMIN_PROJECT_LIST, emptyService.findAll());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), "", USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), USER_PROJECT1.getId(), "", USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.updateById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        @NotNull final String name = USER_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USER_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        service.updateById(USER_TEST.getId(), USER_PROJECT1.getId(), name, description);
        Assert.assertEquals(name, USER_PROJECT1.getName());
        Assert.assertEquals(description, USER_PROJECT1.getDescription());
    }

    @Test
    public void updateByIndex() {
        @Nullable final Project project = service.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        final int index = service.findAll().indexOf(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex(null, index, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex("", index, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), -2, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), index, null, USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), index, "", USER_PROJECT1.getDescription());
        });
        @NotNull final String name = USER_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USER_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        service.updateByIndex(USER_TEST.getId(), index, name, description);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

}
