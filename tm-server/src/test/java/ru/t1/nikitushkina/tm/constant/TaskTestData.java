package ru.t1.nikitushkina.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.comparator.NameComparator;
import ru.t1.nikitushkina.tm.model.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static Task USER_TASK1 = new Task();

    @NotNull
    public final static Task USER_TASK2 = new Task();

    @NotNull
    public final static Task ADMIN_TASK1 = new Task();

    @Nullable
    public final static Task NULL_TASK = null;

    @NotNull
    public final static String NON_EXISTING_TASK_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<Task> USER_TASK_LIST = Arrays.asList(USER_TASK1, USER_TASK2);

    @NotNull
    public final static List<Task> ADMIN_TASK_LIST = Arrays.asList(ADMIN_TASK1);

    @NotNull
    public final static List<Task> TASK_LIST = new ArrayList<>();

    @NotNull
    public final static List<Task> SORTED_TASK_LIST = new ArrayList<>();

    static {
        USER_TASK_LIST.forEach(task -> task.setUserId(UserTestData.USER_TEST.getId()));
        USER_TASK_LIST.forEach(task -> task.setName("User Test Task " + task.getId()));
        USER_TASK_LIST.forEach(task -> task.setDescription("User Test Task " + task.getId() + " description"));
        USER_TASK_LIST.forEach(task -> task.setProjectId(ProjectTestData.USER_PROJECT1.getId()));
        ADMIN_TASK_LIST.forEach(task -> task.setUserId(UserTestData.ADMIN_TEST.getId()));
        ADMIN_TASK_LIST.forEach(task -> task.setName("Admin Test Task " + task.getId()));
        ADMIN_TASK_LIST.forEach(task -> task.setDescription("Admin Test Task " + task.getId() + " description"));
        TASK_LIST.addAll(USER_TASK_LIST);
        TASK_LIST.addAll(ADMIN_TASK_LIST);
        SORTED_TASK_LIST.addAll(TASK_LIST);
        SORTED_TASK_LIST.sort(NameComparator.INSTANCE);
    }

}
