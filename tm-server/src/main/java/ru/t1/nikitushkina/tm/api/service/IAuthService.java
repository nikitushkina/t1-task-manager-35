package ru.t1.nikitushkina.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.Session;
import ru.t1.nikitushkina.tm.model.User;

import javax.naming.AuthenticationException;

public interface IAuthService {

    @NotNull
    @SneakyThrows
    String login(@Nullable String login, @Nullable String password) throws AuthenticationException;

    void logout(@Nullable Session session);

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    Session validateToken(@Nullable String token);

}
