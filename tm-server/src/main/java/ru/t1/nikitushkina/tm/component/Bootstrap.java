package ru.t1.nikitushkina.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.endpoint.*;
import ru.t1.nikitushkina.tm.api.repository.IProjectRepository;
import ru.t1.nikitushkina.tm.api.repository.ISessionRepository;
import ru.t1.nikitushkina.tm.api.repository.ITaskRepository;
import ru.t1.nikitushkina.tm.api.repository.IUserRepository;
import ru.t1.nikitushkina.tm.api.service.*;
import ru.t1.nikitushkina.tm.endpoint.*;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.model.Project;
import ru.t1.nikitushkina.tm.model.User;
import ru.t1.nikitushkina.tm.repository.ProjectRepository;
import ru.t1.nikitushkina.tm.repository.SessionRepository;
import ru.t1.nikitushkina.tm.repository.TaskRepository;
import ru.t1.nikitushkina.tm.repository.UserRepository;
import ru.t1.nikitushkina.tm.service.*;
import ru.t1.nikitushkina.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.nikitushkina.tm.command";
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);
    @Getter
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();
    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);
    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);
    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);
    @NotNull
    private final Backup backup = new Backup(this);
    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);
    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void initBackup() {
        backup.start();
    }

    void initDemoData() {
        @NotNull final User user1 = userService.create("user1", "user1pwd", "user1@a.ru");
        @NotNull final User user2 = userService.create("user2", "user2pwd", "user2@a.ru");
        @NotNull final User user3 = userService.create("user3", "user3pwd", Role.ADMIN);

        projectService.add(user1.getId(), new Project("Project 02", "p02", Status.NOT_STARTED));
        projectService.add(user2.getId(), new Project("Project 03", "p03", Status.COMPLETED));
        projectService.add(user3.getId(), new Project("Project 01", "p01", Status.IN_PROGRESS));

        taskService.create(user1.getId(), "Task 03", "t03");
        taskService.create(user2.getId(), "Task 01", "t01");
        taskService.create(user3.getId(), "Task 02", "t02");
    }

    private void initLogger() {
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
        backup.stop();
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() {
        initPID();
        initDemoData();
        initBackup();
        initLogger();
    }

}
